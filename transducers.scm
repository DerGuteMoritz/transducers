(module transducers

(mapping
 filtering
 deduping
 deduping-consecutive
 deduping-total
 concatenating
 combine
 current-transduction
 transduction
 taking
 dropping
 taking-while
 dropping-while
 keeping)

(import chicken)
(import scheme)
(use srfi-69)
(use data-structures)


(define (((mapping proc) kons) x a)
  (kons (proc x) a))

(define (((filtering pred) kons) x a)
  (if (pred x)
      (kons x a)
      a))

;; This variant assumes that the accumulator contains enough information to tell apart duplicates
(define (((deduping member?) kons) x a)
  (if (member? x a)
      a
      (kons x a)))

;; This variant deduplicates consecutive sequences of the same value in the input
(define ((deduping-consecutive eq) kons)
  (let ((prev '()))
    (lambda (x a)
      (cond ((and (pair? prev) (eq (car prev) x))
             a)
            (else
             (set! prev (list x))
             (kons x a))))))

(define ((deduping-total eq hash) kons)
  (let ((seen (make-hash-table eq hash)))
    (lambda (x a)
      (cond ((hash-table-ref/default seen x #f)
             a)
            (else
             (hash-table-set! seen x #t)
             (kons x a))))))

(define (((concatenating fold) kons) x a)
  (fold kons a x))

(define (combine transducer ?kons . more)
  (let loop ((t+k (cons transducer (cons ?kons more))))
    (if (null? (cdr t+k))
        (car t+k)
        ((car t+k) (loop (cdr t+k))))))

(define current-transduction
  (make-parameter values))

(define-syntax transduction
  (syntax-rules ()
    ((_ body ...)
     (call/cc
      (lambda (ct)
        (parameterize ((current-transduction ct))
          body ...))))))

(define ((taking n) kons)
  (let ((i 0)
        (break (current-transduction)))
    (lambda (x a)
      (cond ((< i n)
             (set! i (+ i 1))
             (kons x a))
            (else (break a))))))

(define ((dropping n) kons)
  (let ((n n))
    (lambda (x a)
      (cond ((zero? n)
             (kons x a))
            (else
             (set! n (- n 1))
             a)))))


(define ((taking-while pred) kons)
  (let ((done? #f)
        (break (current-transduction)))
    (lambda (x a)
      (cond (done?
             (break a))
            ((pred x)
             (kons x a))
            (else
             (set! done? #t)
             (break a))))))


(define ((dropping-while pred) kons)
  (let ((done? #f))
    (lambda (x a)
      (unless (or done? (pred x))
        (set! done? #t))
      (if done?
          (kons x a)
          a))))

(define (((keeping pred) kons) x a)
  (let ((y (pred x)))
    (if y (kons y a) a)))

)
