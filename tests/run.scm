(load-relative "../transducers")
(import transducers)

(use test)
(use srfi-1)

(test-begin)

(test-group "mapping"
  (test 15 (fold ((mapping add1) +) 0 (iota 5))))

(test-group "filtering"
  (test '(1 3 5 7 9) (fold-right ((filtering odd?) cons) '() (iota 10))))


(test-group "composition"
  (let* ((input (iota 5))
         (result (fold + 0 (map add1 (filter odd? input)))))
    (test result (fold ((filtering odd?) ((mapping add1) +)) 0 input))
    (test result (fold ((o (filtering odd?) (mapping add1)) +) 0 input))
    (test result (fold (combine (filtering odd?) (mapping add1) +) 0 input))))

(let ((dups1 '(2 2 2 2 2 2 4 3 3 3 3 3 3))
      (dups2 '(2 2 2 1 1 1 1 2 2)))

  (test-group "deduping"
    (test '(3 4 2) (fold ((deduping memq) cons) '() dups1))
    (test '(1 2) (fold ((deduping memq) cons) '() dups2)))

  (test-group "deduping-consecutive"
    (test '(3 4 2) (fold ((deduping-consecutive =) cons) '() dups1))
    (test '(2 1 2) (fold ((deduping-consecutive =) cons) '() dups2)))

  (test-group "deduping-total"
    (test '(3 4 2) (fold ((deduping-total eqv? eqv?-hash) cons) '() dups1))
    (test '(1 2) (fold ((deduping-total eqv? eqv?-hash) cons) '() dups2))))

(define invocations 0)

(define (counting-invocations proc)
  (set! invocations 0)
  (lambda args
    (set! invocations (+ invocations 1))
    (apply proc args)))

(test-group "taking"
  (test 10 (fold (counting-invocations ((taking 5) +)) 0 (iota 10)))
  (test 10 invocations)
  (test 10 (transduction (fold (counting-invocations ((taking 5) +)) 0 (iota 10))))
  (test 6 invocations))

(test-group "dropping"
  (test 7 (fold ((dropping 3) +) 0 (iota 5))))

(test-group "concatenating"
  (test '(6 5 5 4 4 3)
        (fold-right (combine (mapping (lambda (x) (list x x)))
                             (concatenating fold)
                             (mapping (let ((i 0))
                                        (lambda (x)
                                          (set! i (add1 i))
                                          (+ x i))))
                             cons)
                    '()
                    (iota 3))))

(test-group "taking-while"
  (test '(3 1) (fold (counting-invocations ((taking-while odd?) cons)) '() '(1 3 4 5)))
  (test invocations 4)
  (test '(3 1) (transduction (fold (counting-invocations ((taking-while odd?) cons)) '() '(1 3 4 5))))
  (test invocations 3))


(test-group "dropping-while"
  (test '(7 6 5) (fold ((dropping-while even?) cons) '() '(0 2 4 5 6 7))))

(test-group "keeping"
  (test '((b c d)
          (b c d)
          (a b c d)
          (d))
        (fold ((keeping (lambda (x) (memq x '(a b c d)))) cons)
              '()
              '(d y a x b z z b))))

(test-end)

(test-exit)
